package com.neeraj.rxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    TextView tv_message;
    Button bt_submit;
    private List<String> list;
    private io.reactivex.Observable<List<String>> observable;
    private Observer<List<String>> observer;

    final String TAG=MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUi();

        list=new ArrayList<>();
        list.add("Hi");
        list.add("Neeraj");


        observable= io.reactivex.Observable.just(list);

        observer=new Observer<List<String>>() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.i(TAG,"onSubscribe");
            }

            @Override
            public void onNext(List<String> list) {
                Log.i(TAG,"onNext");
                String msg="";
                for (String s:list){
                    msg=msg+"\n"+s;
                }

                tv_message.setText(msg);
            }

            @Override
            public void onError(Throwable e) {
                Log.i(TAG,"onError");
            }

            @Override
            public void onComplete() {
                Log.i(TAG,"onComplete");
            }
        };


    }

    private void initUi() {

        tv_message=findViewById(R.id.tv_message);
        bt_submit=findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_submit:
                observable.subscribe(observer);
                Log.i(TAG,"onClick");
                break;
        }
    }


}
